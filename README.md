# Mars Explorer

## Installation

### Via Docker

If you want to run the app using *Docker* and and *docker-compose* make sure you have them both installed on your system.
Then simply go to the project main directory and run `docker-compose up` command and direct your browser to `localhost:8521`.

### Via Virtualenv

1. Virtualenv creation:

If you prefer the *virtualenv* apprach you need to create one either by:
`virtualenv {env_name}` with `{env_name}` being whatever you'd like to name your virtualenv.  
Alternatively with *virtualenvwrapper* you can simply type:
`mkvirtualenv {env_name}` with `{env_name}` being whatever you'd like it to.

However, please keep in mind that this project requires **python3** so you may need to add  
`-p {path_to_python3_in_your_system}`  
to either of these commands making them look like this:  
`virtualenv -p {path_to_python3_in_your_system} {env_name}`  
or:  
`mkvirtualenv -p {path_to_python3_in_your_system} {env_name}`.  

2. Requirements installation:

With your virtualenv active you need to install the requirements next.
To do so simply type:  
`pip install -r requirements.txt`  
in the project's directory.

3. Project startup:

To actually run the simulation you need to start it by running:
`python visualization.py`
And open `localhost:8521` in your browser.
