from mesa.visualization.modules import CanvasGrid
from mesa.visualization.ModularVisualization import ModularServer

from agents import RobotAgent
from models import MarsModel
from space import BCOSSingleGrid


def agent_portrayal(cell_content):
    portrayal = None
    if isinstance(cell_content, RobotAgent):
        portrayal = {
            "Shape": "circle",
            "Filled": "true",
            "Layer": 1,
            "Color": "purple" if cell_content.has_sample() else "red",
            "r": 0.5,
        }
    elif isinstance(cell_content, BCOSSingleGrid.Base):
        portrayal = {
            "Shape": "rect",
            "Filled": "false",
            "Layer": 0,
            "Color": "blue",
            "w": 0.9,
            "h": 0.9,
            "text": "BASE",
            "text_color": "white",
        }
    elif isinstance(cell_content, BCOSSingleGrid.Crumb):
        portrayal = {
            "Shape": "circle",
            "Filled": "true",
            "Layer": 2,
            "Color": "yellow",
            "r": 0.2,
            "text": str(cell_content.amount),
            "text_color": "black",
        }
    elif isinstance(cell_content, BCOSSingleGrid.Obstacle):
        portrayal = {
            "Shape": "rect",
            "Filled": "true",
            "Layer": 0,
            "Color": "black",
            "w": 1,
            "h": 1,
        }
    elif isinstance(cell_content, BCOSSingleGrid.Sample):
        portrayal = {
            "Shape": "rect",
            "Filled": "true",
            "Layer": 0,
            "Color": "green",
            "w": 0.2,
            "h": 0.2,
        }
    return portrayal


grid = CanvasGrid(agent_portrayal, 10, 10, 500, 500)
server = ModularServer(
    MarsModel,
    [grid],
    "Mars Model",
    10,
    10,
    10,
)
server.port = 8521  # The default
server.launch()
