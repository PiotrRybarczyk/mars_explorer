from random import choice

from mesa.space import SingleGrid, accept_tuple_argument


class BCOSSingleGrid(SingleGrid):
    """
    Single Grid with additional data of Base, Crumbs, Obstacles and Samples.
    """

    class Base:
        pass

    class Crumb:

        def __init__(self, amount, *args, **kwargs):
            self.amount = amount
            super().__init__(*args, **kwargs)

    class Obstacle:
        pass

    class Sample:
        pass

    def __init__(
        self,
        width,
        height,
        torus,
        base_coords,
        num_obstacles=0,
        num_samples=10,
    ):
        """ Create a new single-item grid with samples and crumbs grids.

        Args:
            width, height: The width and width of the grid
            torus: Boolean whether the grid wraps or not.

        """
        super().__init__(width, height, torus)

        # Initiate Samples and Crumbs grids.
        self.crumb_drop_amount = 2
        self.crumb_pick_amount = 1
        self.base = base_coords
        self.base_points = []
        self.crumbs = []
        self.obstacles = []
        self.samples = []

        x_min, y_min, x_max, y_max = self.base
        for x in range(x_min, x_max + 1):
            for y in range(y_min, y_max + 1):
                pos = x, y
                self.base_points.append(pos)
                if pos in self.empties:
                    self.empties.remove(pos)

        for x in range(self.width):
            crumbs_col = []
            obstacles_col = []
            sample_col = []
            for y in range(self.height):
                sample_col.append(self.sample_default_val())
                obstacles_col.append(self.obstacles_default_val())
                crumbs_col.append(self.crumb_default_val())
            self.samples.append(sample_col)
            self.obstacles.append(obstacles_col)
            self.crumbs.append(crumbs_col)

        if num_obstacles:
            while num_obstacles and self.exists_empty_cells():
                self._place_obstacle(self.find_empty())
                num_obstacles -= 1

        while num_samples and self.exists_empty_cells():
            self._place_sample(self.find_empty())
            num_samples -= 1

    @staticmethod
    def sample_default_val():
        """ Default value for new sample cell elements. """
        return None

    @staticmethod
    def obstacles_default_val():
        """ Default value for new obstacle cell elements. """
        return None

    @staticmethod
    def crumb_default_val():
        """ Default value for new crumb cell elements. """
        return 0

    @accept_tuple_argument
    def iter_cell_list_contents(self, cell_list):
        """
        Args:
            cell_list: Array-like of (x, y) tuples, or single tuple.

        Returns:
            An iterator of the contents of the cells identified in cell_list

        """
        agents = super().iter_cell_list_contents(cell_list)
        base, crumbs, obstacles, samples = [], [], [], []
        for x, y, in cell_list:
            if self.is_base((x, y)):
                base.append(self.Base())
            if self.has_crumbs((x, y)):
                crumbs.append(self.Crumb(self.crumbs[x][y]))
            if self.has_obstacle((x, y)):
                obstacles.append(self.Obstacle())
            if self.has_sample((x, y)):
                samples.append(self.Sample())
        return list(agents) + base + crumbs + obstacles + samples

    def _place_obstacle(self, pos):
        """ Place the obstacle at the correct location. """
        x, y = pos
        self.obstacles[x][y] = self.Obstacle()
        if pos in self.empties:
            self.empties.remove(pos)

    def _place_sample(self, pos):
        """ Place the sample at the correct location. """
        x, y = pos
        self.samples[x][y] = self.Sample()

    def is_cell_empty(self, pos):
        robots_presence = super().is_cell_empty(pos)
        x, y = pos
        obstacles_presence = (
            True if self.obstacles[x][y] == self.obstacles_default_val()
            else False
        )
        return robots_presence and obstacles_presence

    def has_crumbs(self, pos):
        x, y = pos
        return bool(self.crumbs[x][y])

    def has_obstacle(self, pos):
        x, y = pos
        return bool(self.obstacles[x][y])

    def drop_crumbs(self, pos):
        x, y = pos
        self.crumbs[x][y] += self.crumb_drop_amount

    def pick_crumbs(self, pos):
        x, y = pos
        self.crumbs[x][y] -= self.crumb_pick_amount
        if self.crumbs[x][y] < self.crumb_default_val():
            self.crumbs[x][y] = self.crumb_default_val()

    def get_crumbs_amount(self, pos):
        x, y = pos
        return self.crumbs[x][y]

    def has_sample(self, pos):
        x, y = pos
        return bool(self.samples[x][y])

    def pick_sample(self, pos):
        x, y = pos
        if not self.has_sample(pos):
            raise Exception("ERROR: No sample on this cell")
        self.samples[x][y] = self.sample_default_val()
        return self.Sample()

    def drop_sample(self, pos):
        x, y = pos
        if not self.is_base(pos):
            raise Exception("ERROR: Tried to drop sample outside of base")
        return self.sample_default_val()

    def is_base(self, pos):
        x, y = pos
        x1, y1, x2, y2 = self.base
        return x1 <= x <= x2 and y1 <= y <= y2

    def get_two_pos_distance(self, pos1, pos2):
        x1, y1 = pos1
        x2, y2 = pos2
        return max([abs(x2 - x1), abs(y2 - y1)])

    def get_base_distance(self, pos):
        return min(
            [
                self.get_two_pos_distance(pos, base_p)
                for base_p in self.base_points
            ]
        )

    def get_pos_with_min_base_distance(self, pos_list):
        min_index, min_value = min(
            enumerate([self.get_base_distance(pos) for pos in pos_list]),
            key=lambda m: m[1],
        )
        return pos_list[min_index]

    def get_pos_with_sample(self, pos_list):
        for pos in pos_list:
            if self.has_sample(pos):
                return pos
        return None

    def get_pos_with_max_crumbs(self, pos_list):
        max_index, max_value = max(
            enumerate([self.get_crumbs_amount(pos) for pos in pos_list]),
            key=lambda m: m[1],
        )
        return pos_list[max_index]
