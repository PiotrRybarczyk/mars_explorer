FROM python:3.6

WORKDIR /mars_explorer
ADD . /mars_explorer
RUN pip install -r requirements.txt
