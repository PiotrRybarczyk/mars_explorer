from mesa import Model
from mesa.time import RandomActivation

from agents import RobotAgent
from space import BCOSSingleGrid


class MarsModel(Model):
    """
    Model for mars environment.
    """

    def __init__(self, num_agents=10, width=100, height=100):
        self.num_agents = num_agents
        self.grid = BCOSSingleGrid(width, height, False, (5, 5, 5, 5), 20)
        self.schedule = RandomActivation(self)
        self.running = True
        for i in range(self.num_agents):
            agent = RobotAgent(i, self)
            self.schedule.add(agent)
            self.grid.position_agent(agent)

    def step(self):
        self.schedule.step()
