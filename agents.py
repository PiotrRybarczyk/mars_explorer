import random

from mesa import Agent


class RobotAgent(Agent):
    """
    Agent class for mars roaming robots.
    """

    def __init__(self, unique_id, model):
        super().__init__(unique_id, model)
        self.sample = None

    def has_sample(self):
        return bool(self.sample)

    def pos_has_sample(self):
        return self.model.grid.has_sample(self.pos)

    def pick_sample(self):
        self.sample = self.model.grid.pick_sample(self.pos)

    def drop_sample(self):
        self.sample = self.model.grid.drop_sample(self.pos)

    def is_at_base(self):
        return self.model.grid.is_base(self.pos)

    def pos_has_crumbs(self):
        return self.model.grid.has_crumbs(self.pos)

    def drop_crumbs(self):
        return self.model.grid.drop_crumbs(self.pos)

    def pick_crumbs(self):
        return self.model.grid.pick_crumbs(self.pos)

    def get_neighbors(self):
        neighbors = self.model.grid.get_neighborhood(
            self.pos,
            moore=True,
            include_center=False,
        )
        return [n for n in neighbors if self.model.grid.is_cell_empty(n)]

    def move_to_pos(self, pos):
        self.model.grid.move_agent(self, pos)

    def random_move(self):
        neighbors = self.get_neighbors()
        try:
            new_position = random.choice(neighbors)
        except IndexError:
            return
        self.move_to_pos(new_position)

    def move_closer_to_base(self):
        neighbors = self.get_neighbors()
        new_pos = self.model.grid.get_pos_with_min_base_distance(neighbors)
        self.move_to_pos(new_pos)

    def move_where_more_crumbs_or_sample(self):
        neighbors = self.get_neighbors()
        pos_with_sample = self.model.grid.get_pos_with_sample(neighbors)
        if pos_with_sample:
            self.move_to_pos(pos_with_sample)
        else:
            pos_with_max_crumbs = self.model.grid.get_pos_with_max_crumbs(
                neighbors)
            self.move_to_pos(pos_with_max_crumbs)

    def step(self):
        if self.is_at_base() and self.has_sample():
            self.drop_sample()
        elif not self.is_at_base() and self.has_sample():
            self.drop_crumbs()
            self.move_closer_to_base()
        elif self.pos_has_sample():
            self.pick_sample()
            self.drop_crumbs()
            self.move_closer_to_base()
        elif self.pos_has_crumbs():
            self.pick_crumbs()
            self.move_where_more_crumbs_or_sample()
        self.random_move()
